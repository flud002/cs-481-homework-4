﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Homework_4
{
    //Resources Used: https://www.c-sharpcorner.com/article/listview-and-creating-list-in-xamarin/
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public class Destination
    {
        public string Name
        {
            get;
            set;
        }
        public string Category
        {
            get;
            set;
        }
        public string Image
        {
            get;
            set;
        }
    }
        public partial class MainPage : ContentPage
         {
            public ObservableCollection<Destination> List1;

        public MainPage()
        {
            
            InitializeComponent();
            this.BindingContext = this;
            this.BackgroundImage = "skybackground.jpg";
            AddCells();
            DisplayAlert("Welcome!","Look through our various travel destinations. You may add or delete items by holding down on the item then choosing the corresponding button.", "Ok");
            
            TravelDestinations.RefreshCommand = new Command(() => {
                RefreshData();
                TravelDestinations.IsRefreshing = false;
            });
        }
        public void RefreshData()
        {
            TravelDestinations.ItemsSource = null;
            AddCells();
        }
        void AddCells()
        {
            List1 = new ObservableCollection<Destination>
            {
                new Destination
                {
                    Name = "Maui",Category = "Tags:Tropical, Isolated ",Image = "hawaii.jpg"       
                },
                new Destination
                {
                    Name = "Bora Bora",Category = "Tags:Tropical, Isolated",Image = "borabora.jpg"
                },
                new Destination
                {
                    Name = "Florence",Category = "Tags:European, Historical",Image = "florence.jpg"
                },
                new Destination
                {
                    Name = "Prague",Category = "Tags:European, Historical",Image = "prauge.jpg"
                },
                new Destination
                {
                    Name = "Tokyo",Category = "Tags:Asian, Metropolitan",Image = "japan.jpg"
                },
                new Destination
                {
                    Name = "Phuket",Category = "Tags:Asian, Tropical",Image = "thailand.jpg"
                },
                new Destination
                {
                    Name = "Yosemite",Category = "Tags:Mountains, Isolated",Image = "yosemite.jpg"
                },
                new Destination
                {
                    Name = "Barcelona",Category = "Tags:European, Metropolitan",Image = "barcelona.jpg"
                },
                new Destination
                {
                    Name = "Dubai",Category = "Tags:Middle East, Metropolitan",Image = "dubai.jpg"
                },
                new Destination
                {
                    Name = "Sydney",Category = "Tags:Australia, Metropolitan",Image = "sydney.jpg"
                },
                new Destination
                {
                    Name = "Maldives",Category = "Tags:Tropical, Isolated",Image = "maldives.jpg"
                },
                new Destination
                {
                    Name = "Amsterdam",Category = "Tags:European, Historical",Image = "amsterdam.jpg"
                },
                new Destination
                {
                    Name = "Banff",Category = "Tags:Forrest, Isolated",Image = "banff.jpg"
                }
            };
            TravelDestinations.ItemsSource = List1;
        }
        async void OnLearnClicked(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            string name = button.ClassId;
            string item = button.Text;
            
            if (name == "Maui")
            {
                item = "Viewed";
                await Navigation.PushAsync(new MauiPage());
            }
            if(name =="Bora Bora")
            {
                await Navigation.PushAsync(new BoraBoraPage());
            }
            if (name == "Florence")
            {
                await Navigation.PushAsync(new FlorencePage());
            }
            if (name == "Prague")
            {
                await Navigation.PushAsync(new PraguePage());
            }
            if (name == "Tokyo")
            {
                await Navigation.PushAsync(new TokyoPage());
            }
            if (name == "Phuket")
            {
                await Navigation.PushAsync(new PhuketPage());
            }
            if(name == "Yosemite")
            {
                await Navigation.PushAsync(new YosemitePage());
            }
            if (name == "Barcelona")
            {
                await Navigation.PushAsync(new BarcelonaPage());
            }
            if (name == "Dubai")
            {
                await Navigation.PushAsync(new DubaiPage());
            }
            if (name == "Sydney")
            {
                await Navigation.PushAsync(new SydneyPage());
            }
            if (name == "Maldives")
            {
                await Navigation.PushAsync(new MaldivesPage());
            }
            if (name == "Amsterdam")
            {
                await Navigation.PushAsync(new AmsterdamPage());
            }
            if (name == "Banff")
            {
                await Navigation.PushAsync(new BanffPage());
            }

        }
        void OnAdd(object sender, EventArgs e)
        {
            var item1 = (MenuItem)sender;
            var item = (Destination)item1.CommandParameter;
            if (item1 != null)
            {
                DisplayAlert("Alert", "You Have Added Another " + item.Name + " Cell.", "Ok");
                List1.Add(item);
            }
        }
        void OnDelete(object sender, EventArgs e)
        {
            var item1 = (MenuItem)sender;
            var item = (Destination)item1.CommandParameter;
            if (item1 != null)
            {
                DisplayAlert("Alert", "You Have Deleted " + item.Name + " Cell.", "Ok");
                List1.Remove(item);
            }

        }
    }

}
