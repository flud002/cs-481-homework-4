﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Homework_4
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MauiPage : ContentPage
    {
        public MauiPage()
        {
            this.BackgroundImage = "peachback.jpg";
            InitializeComponent();
        }
    }
}