﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Homework_4
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SydneyPage : ContentPage
    {
        public SydneyPage()
        {
            this.BackgroundImage = "peachback.jpg";
            InitializeComponent();
        }
    }
}